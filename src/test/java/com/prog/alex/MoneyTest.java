package com.prog.alex;

import com.prog.alex.exceptions.MoneyMismatchException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Unit test for simple App.
 */
public class MoneyTest {

    /**
     * Comprobar que al crear una moneda, se establece correctamente sus atributos
     */
    @Test
    void testCreateMoney() {

        Money money = new Money(100, "EUR");

        Assertions.assertEquals(money.getAmount(), 100, "ERROR");
        Assertions.assertEquals(money.getCurrencyISOCode(), "EUR", "Tipo de moneda erronea");

    }

    /**
     * Comprobar que podemos sumar monedas del mismo tipo
     */
    @Test
    void testAddAmountToMoneySameCurrency() {

        Money prueba1 = new Money(1, "EUR");
        Money prueba2 = new Money(1, "EUR");

        prueba1.plus(prueba2);

        Assertions.assertEquals(prueba1.getAmount(), 2);

    }

    /**
     * Comprobar que si intentamos sumar monedas de distinto tipo se lanza una excepción
     */
    @Test
    void testAddAmountToMoneyMismatchCurrency() {

        Money prueba = new Money(1, "EUR");

        Assertions.assertThrows(MoneyMismatchException.class, () -> prueba.plus(new Money(1, "USD"))); //NO entiendo muy bien la flecha ( preguntar Alex )

    }

    /**
     * Comprobar que podemos restar monedas del mismo tipo
     */
    @Test
    void testSubtractAmountToMoneySameCurrency() {

        Money test = new Money(1, "EUR");
        test.minus(new Money(1, "EUR"));

        Assertions.assertEquals(test.getAmount(), 0);

    }

    /**
     * Comprobar que si intentamos restar monedas de distinto tipo se lanza una excepción
     */
    @ParameterizedTest
    @CsvSource({"EUR,USD", "EUR,GBP", "USD,GBP"})
    void testSubtractAmountToMoneyMismatchCurrency(String moneda1, String moneda2) {

        Money test = new Money(1, moneda1);

        Assertions.assertThrows(MoneyMismatchException.class, () -> test.minus(new Money(1, moneda2)), "ERROR");

    }

    /**
     * Comprobar comparación de monedas iguales
     */
    @Test
    void testCompareMoneysSameCurrency() {

        Money prueba = new Money(1,"EUR");

        Assertions.assertTrue(prueba.equals(new Money(1,"EUR")));
    }

    /**
     * Comprobar comparación de monedas diferentes
     */
    @Test
    void testCompareMoneysMismatchCurrency() {

        Money prueba = new Money(1,"EUR");

        Assertions.assertFalse(prueba.equals(new Money(1,"USD")), "No se detectat monedas parecidas");

    }
}
