package com.prog.alex;

import com.prog.alex.exceptions.CurrencyNotSupportedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


public class BankTest {


    @ParameterizedTest
    @CsvSource({"EUR,USD,1.35", "EUR,GBP,0.9", "USD,EUR,0.83", "USD,GBP,0.74", "GBP,EUR,1.13", "GBP,USD,1.35"})
    void testConvertAmount(String currencyA, String currencyB, double conversion) {

        Money moneyTest = new Money(1, currencyA);
        Bank bank = new Bank();

        Assertions.assertEquals(conversion, bank.convert(moneyTest, currencyB).getAmount());

    }

    @ParameterizedTest
    @CsvSource({"EUR", "GBP", "USD"})
    void testConvertNotSupportedCurrency(String currency) {

        Bank bank = new Bank();
        Money mony = new Money(1, currency);

        Assertions.assertThrows(CurrencyNotSupportedException.class, () -> bank.convert(mony, "TDP"));

    }

}
